#include <SoftwareSerial.h>

SoftwareSerial BT1(D1, D0); // RX | TX
void setup()
   { 
     pinMode(D2, OUTPUT);        // Al poner en HIGH forzaremos el modo AT
     pinMode(D3, OUTPUT);        // cuando se alimente de aqui
     digitalWrite(D3, HIGH);
     delay (500) ;              // Espera antes de encender el modulo
     Serial.begin(9600);
     Serial.println("Levantando el modulo HC-06");
     digitalWrite (D2, HIGH);    //Enciende el modulo
     Serial.println("Esperando comandos AT:");
     BT1.begin(57600); 
   }

void loop()
   {  if (BT1.available())
            Serial.write(BT1.read());
      if (Serial.available())
            BT1.write(Serial.read());
   }
