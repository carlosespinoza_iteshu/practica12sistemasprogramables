﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Practica12
{
    public static class Repositorio
    {
        readonly static string dbName = @"SistemasProgramables.db";
        public static string Error { get; private set; }
        public static List<Punto> Leer
        {
            get
            {
                try
                {
                    List<Punto> puntos;
                    using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                    {
                        puntos = db.GetCollection<Punto>("Puntos").FindAll().ToList();
                    }
                    Error = "";
                    return puntos;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public static Punto Crear(Punto p)
        {
            try
            {
                p.Id = Guid.NewGuid().ToString();
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                    db.GetCollection<Punto>("Puntos").Insert(p);
                }
                Error = "";
                return p;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
        public static bool Eliminar(string id)
        {
            try
            {
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                    if (db.GetCollection<Punto>("Puntos").Delete(id))
                    {
                        Error = "";
                        return true;
                    }
                    else
                    {
                        Error = "No se pudo eliminar el punto";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public static Punto Actualizar(Punto p)
        {
            try
            {
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                   if( db.GetCollection<Punto>("Puntos").Update(p))
                    {
                        Error = "";
                        return p;
                    }
                    else
                    {
                        Error = "No se pudo modificar el punto";
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
    }
}
