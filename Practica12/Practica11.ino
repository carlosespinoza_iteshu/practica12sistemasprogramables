#include <DHT.h>
#define DHTTYPE  DHT11
int tempHum=D4;
int pinLDR=A0;
int senHum=D0;
int trigger=D2;
int echo=D1;
long duracion=0;
int pinRele=D5;
DHT dht(tempHum, DHTTYPE);
float temperatura;
float humedad;
int pinRele2=D3;
int pinPIR=D6;
long tiempo=0;
int hayMovimiento;


void setup()
{
   Serial.begin(9600);
   pinMode(pinLDR,INPUT); //Analogico
   pinMode(senHum,INPUT); //Digital
   pinMode(trigger,OUTPUT);
   pinMode(echo,INPUT);
   pinMode(pinRele,OUTPUT);
   pinMode(pinRele2,OUTPUT);
   pinMode(pinPIR,INPUT);
   tiempo=millis();
   hayMovimiento=0;
}

void loop()
{
  VerificaMovimiento();
  VerificaComando();
  if((millis()-tiempo)>30000){
    tiempo=millis();
    Serial.print(HumedadAmbiental());  //humedad
    Serial.print(" "); Serial.print(Temperatura()); //temperatura
    Serial.print(" "); Serial.print(Luminosidad()); //Analogico fotorestistencia
    Serial.print(" "); Serial.print(HumedadTierra()); //Digital Humedad
    Serial.print(" "); Serial.println(Distancia()); //distancia ultrasonico
  }
}

void VerificaComando(){
  if(Serial.available())
  {
    char comando[20];
    size_t count = Serial.readBytesUntil('\n', comando, 20);
  
    Serial.print(comando[0]);
    Serial.println(comando[1]);
    switch(comando[0]){
      case 'T':
        if(comando[1]=='?'){
          Serial.print("T=");
          Serial.println(Temperatura());
        }
      break;
      case 'h':
        if(comando[1]=='?'){
          Serial.print("h=");
          Serial.println(HumedadTierra());
        }
      break;
      case 'H':
        if(comando[1]=='?'){
          Serial.print("H=");
          Serial.println(HumedadAmbiental());
        }
      break;
      case 'D':
        if(comando[1]=='?'){
          Serial.print("D=");
          Serial.println(Distancia());
        }
      break;
      case 'L':
        if(comando[1]=='?'){
          Serial.print("L=");
          Serial.println(Luminosidad());
        }
      break;
      case 'F':
        if(comando[1]=='1'){
          Foco(1);
        }
        if(comando[1]=='0'){
          Foco(0);
        }
      break;
      case 'S':
        if(comando[1]=='?'){
          Serial.print(HumedadAmbiental());  //humedad
          Serial.print(" "); Serial.print(Temperatura()); //temperatura
          Serial.print(" "); Serial.print(Luminosidad()); //Analogico fotorestistencia
          Serial.print(" "); Serial.print(HumedadTierra()); //Digital Humedad
          Serial.print(" "); Serial.println(Distancia()); //distancia ultrasonico
        }
      break;
    }
  }
}

void VerificaMovimiento(){
  if(digitalRead(pinPIR)==HIGH){
    digitalWrite(pinRele2,HIGH);
      if(hayMovimiento==0){
        Serial.println("M");
        hayMovimiento=1;
      }
  }else{
    digitalWrite(pinRele2,LOW);
    hayMovimiento=0;
  }
}

int Temperatura(){
  float t;
  t=dht.readTemperature();
  if(!isnan(t))
  {
    temperatura=t;
  }
  return temperatura;
}

int HumedadAmbiental(){
  float h;
  h=dht.readHumidity();
  if(!isnan(h)){
      humedad=h;
  }
  return humedad;
}

int HumedadTierra(){
  if(digitalRead(senHum)==HIGH){
    return 1;
  }else{
    return 0;
  }
}

int Luminosidad(){
  return analogRead(pinLDR);
}

int Distancia(){
  digitalWrite(trigger,LOW);
  delayMicroseconds(2);
  digitalWrite(trigger,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger,LOW);
  duracion=pulseIn(echo,HIGH);
  return Distance(duracion);
}

long Distance(long time){
  long DistanceCalc;
  DistanceCalc=((time/2.9)/2); //distancia en mm
  return DistanceCalc/10; //regresa distancia en cm  
}

void Foco(int valor){
  if(valor==1){
      digitalWrite(pinRele,HIGH);
    }else{
      digitalWrite(pinRele,LOW);
    }
}
