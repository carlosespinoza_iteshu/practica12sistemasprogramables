﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using System.Windows.Threading;
using OxyPlot;
using OxyPlot.Axes;

namespace Practica12
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SerialPort puerto;
        string datoRecibido="";
        DispatcherTimer timer;
        List<Punto> Puntos;
        public MainWindow()
        {
            InitializeComponent();
            cmbPuertos.ItemsSource = SerialPort.GetPortNames();
            HabilitarBotones(false);
            timer = new DispatcherTimer();
            timer.Tick += Timer_Tick;
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
            Puntos = new List<Punto>();
            LeerDeBD();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (datoRecibido != "")
                {
                    // datoRecibido = datoRecibido.Replace("\n","").Replace("\r","");
                    lstLog.Items.Add($"{DateTime.Now.ToShortTimeString()}: {datoRecibido}");
                    if (datoRecibido == "M\r\n")
                    {
                        lbtUltimoMovimiento.Content = DateTime.Now.ToLongTimeString();
                    }
                    //44 24 99 1 5 -> hA t l ht d
                    if (datoRecibido.Length > 10)
                    {
                        string[] datos = datoRecibido.Split(' ');
                        if (datos.Length == 5)
                        {
                            Punto p = new Punto()
                            {
                                Hora = DateTime.Now,
                                Distancia = int.Parse(datos[4]),
                                HumAmbiental = int.Parse(datos[0]),
                                HumTierra = int.Parse(datos[3]),
                                Luminosidad = int.Parse(datos[2]),
                                Temperatura = int.Parse(datos[1])

                            };
                            Puntos.Add(p);
                            Graficar();
                            Repositorio.Crear(p);
                            dtgValores.ItemsSource = null;
                            dtgValores.ItemsSource = Puntos;
                            LeerDeBD();
                        }
                    }
                    datoRecibido = "";
                }
            }
            catch (Exception ex)
            {
                lstLog.Items.Add(ex.Message);
            }
            
        }

        private void LeerDeBD()
        {
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = Repositorio.Leer;
        }

        private void Graficar()
        {
            PlotModel model = new PlotModel()
            {
                Title = "Datos de sensores"
            };

            DateTimeAxis ejex = new DateTimeAxis();
            OxyPlot.Series.LineSeries temperatura = new OxyPlot.Series.LineSeries();
            OxyPlot.Series.LineSeries humTierra = new OxyPlot.Series.LineSeries();
            OxyPlot.Series.LineSeries humAmbiental = new OxyPlot.Series.LineSeries();
            OxyPlot.Series.LineSeries luminosidad = new OxyPlot.Series.LineSeries();
            OxyPlot.Series.LineSeries distancia = new OxyPlot.Series.LineSeries();

            foreach (var item in Puntos)
            {
                temperatura.Points.Add(DateTimeAxis.CreateDataPoint(item.Hora, item.Temperatura));
                humAmbiental.Points.Add(DateTimeAxis.CreateDataPoint(item.Hora, item.HumAmbiental));
                humTierra.Points.Add(DateTimeAxis.CreateDataPoint(item.Hora, item.HumTierra));
                luminosidad.Points.Add(DateTimeAxis.CreateDataPoint(item.Hora, item.Luminosidad));
                distancia.Points.Add(DateTimeAxis.CreateDataPoint(item.Hora, item.Distancia));
            }
            temperatura.Title = "Temperatura °C";
            humAmbiental.Title = "Humedad Ambiental %";
            humTierra.Title = "Humedad en tierra";
            luminosidad.Title = "Luminosidad lum";
            distancia.Title = "Distancia cm";
            model.Axes.Add(ejex);
            model.Series.Add(temperatura);
            model.Series.Add(distancia);
            model.Series.Add(humTierra);
            model.Series.Add(humAmbiental);
            model.Series.Add(luminosidad);

            Grafico.Model = null;
            Grafico.Model = model;
        }

        private void HabilitarBotones(bool habilitado)
        {
            stkBotones.IsEnabled = habilitado;
        }

        private void chkConectar_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                puerto = new SerialPort(cmbPuertos.Text);
                puerto.Parity = Parity.None;
                puerto.BaudRate = 9600;
                puerto.StopBits = StopBits.One;
                puerto.ReadTimeout = 1000;
                puerto.WriteTimeout = 1000;
                puerto.Handshake = Handshake.None;
                puerto.ReadBufferSize = 64;
                puerto.WriteBufferSize = 64;
                puerto.DtrEnable = true;
                puerto.RtsEnable = true;
                puerto.Open();
                MessageBox.Show("Conectado correctamente", "Practica 12", MessageBoxButton.OK, MessageBoxImage.Information);
                HabilitarBotones(true);
                puerto.DataReceived += Puerto_DataReceived;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Puerto_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            datoRecibido = puerto.ReadExisting();
        }

        private void chkConectar_Unchecked(object sender, RoutedEventArgs e)
        {
            if (puerto.IsOpen)
            {
                puerto.Close();
                MessageBox.Show("Puerto cerrado correctamente", "Practica 12", MessageBoxButton.OK, MessageBoxImage.Information);
                puerto.DataReceived -= Puerto_DataReceived;
                HabilitarBotones(false);
            }
            else
            {
                MessageBox.Show("El puerto no esta abierto", "Practica 12", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnF1Encender_Click(object sender, RoutedEventArgs e)
        {
            puerto.Write("F1");
        }

        private void btnF1APagar_Click(object sender, RoutedEventArgs e)
        {
            puerto.Write("F0");
        }
    }

    public class Punto
    {
        public string Id { get; set; }
        public DateTime Hora { get; set; }
        public int HumAmbiental { get; set; }
        public int Temperatura { get; set; }
        public int Luminosidad { get; set; }
        public int HumTierra { get; set; }
        public int Distancia { get; set; }

    }
}
